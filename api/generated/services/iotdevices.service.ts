/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { IoTModel } from '../models/io-t-model';
import { IoTModels } from '../models/io-t-models';

@Injectable({
  providedIn: 'root',
})
export class IotdevicesService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation listItems
   */
  static readonly ListItemsPath = '/api';

  /**
   * View all items.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `listItems()` instead.
   *
   * This method doesn't expect any request body.
   */
  listItems$Response(params?: {
  }): Observable<StrictHttpResponse<IoTModels>> {

    const rb = new RequestBuilder(this.rootUrl, IotdevicesService.ListItemsPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IoTModels>;
      })
    );
  }

  /**
   * View all items.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `listItems$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  listItems(params?: {
  }): Observable<IoTModels> {

    return this.listItems$Response(params).pipe(
      map((r: StrictHttpResponse<IoTModels>) => r.body as IoTModels)
    );
  }

  /**
   * Path part for operation createItem
   */
  static readonly CreateItemPath = '/api';

  /**
   * Create a new item.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createItem()` instead.
   *
   * This method doesn't expect any request body.
   */
  createItem$Response(params?: {
  }): Observable<StrictHttpResponse<IoTModel>> {

    const rb = new RequestBuilder(this.rootUrl, IotdevicesService.CreateItemPath, 'post');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IoTModel>;
      })
    );
  }

  /**
   * Create a new item.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createItem$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  createItem(params?: {
  }): Observable<IoTModel> {

    return this.createItem$Response(params).pipe(
      map((r: StrictHttpResponse<IoTModel>) => r.body as IoTModel)
    );
  }

  /**
   * Path part for operation showItemById
   */
  static readonly ShowItemByIdPath = '/api/{id}';

  /**
   * Retrieve specific item with the supplied item id.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `showItemById()` instead.
   *
   * This method doesn't expect any request body.
   */
  showItemById$Response(params: {

    /**
     * Item id.
     */
    id: number;
  }): Observable<StrictHttpResponse<IoTModel>> {

    const rb = new RequestBuilder(this.rootUrl, IotdevicesService.ShowItemByIdPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IoTModel>;
      })
    );
  }

  /**
   * Retrieve specific item with the supplied item id.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `showItemById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  showItemById(params: {

    /**
     * Item id.
     */
    id: number;
  }): Observable<IoTModel> {

    return this.showItemById$Response(params).pipe(
      map((r: StrictHttpResponse<IoTModel>) => r.body as IoTModel)
    );
  }

}
