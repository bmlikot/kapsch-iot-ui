/* tslint:disable */
/* eslint-disable */
export interface IoTModel {
  createdTime?: string;
  device: string;
  name?: string;
  value?: number;
}
