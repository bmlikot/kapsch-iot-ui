import { Component, OnInit } from '@angular/core';
import { IoTModels } from 'api/generated/models';
import { IotdevicesService } from 'api/generated/services';
import { Measurement } from 'src/app/model/measurement';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  chartData: any;
  chartOptions = {
    animation: {
      duration: 0
    }
  };

  constructor(
    private iotdevicesService: IotdevicesService
  ) { }

  ngOnInit(): void {
    this.iotdevicesService.listItems().subscribe((iotModels: IoTModels) => {
      this.updateChartData(iotModels);
    });
  }

   /**
   * Update chart with fetched data.
   * @param measurements array
   * @param deviceId device id
   * @param mType measurement type
   */
  updateChartData(iotModels: IoTModels) {
    if (!iotModels) {
      iotModels = [];
    }

    const labelValues = [];
    const dataValues = [];

    iotModels.forEach((measure: Measurement, index: number) => {
      // Koristimo unshift jer starije podatke guramo na pocetak array-a.
      const date = new Date(measure.createdTime);
      labelValues.unshift(this.formatDate(date));
      dataValues.unshift(measure.value);
    });

    this.chartData = {
      labels: labelValues,
      datasets: [
        {
          label: 'Device1',
          data: dataValues,
          fill: false,
          borderColor: 'blue',
          lineTension: 0
        }
      ]
    };

  }

  formatDate(date: Date) {

    const mm = date.getMonth() + 1;
    const dd = date.getDate();
    const yyyy: string = '' + date.getFullYear();

    let ddStr: string = '' + dd;
    if (dd < 10) {
      ddStr = '0' + dd;
    }

    let mmStr: string = '' + mm;
    if (mm < 10) {
      mmStr = '0' + mm;
    }

    return yyyy.slice(-2) + '/' + mmStr + '/' + ddStr
      + ' ' + date.toLocaleTimeString('hr-HR');
  }

}
