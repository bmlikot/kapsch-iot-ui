export class Measurement {
    constructor(
        public id: string,
        public name: string,
        public value: number,
        public createdTime: string,
        public measurementType: string,
        public device: string
    ) { }
}
